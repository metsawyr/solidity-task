### Implementation details
1. For simplicity purpose inside the TokenExchanger contract consider all supported contracts as ERC20 without any check
2. etherRate in struct `TokenSpecification` is how much 1 token cost in ethers
3. Only one token holder per one supported token allowed
4. Migrations are unimplemented due to late night and my desire to sleep :)

### Logic
In order to execute an exchange you need to follow next requirements:
1. Both source and destination token contracts should be added to `supportedTokens` mapping
2. Exchange contract should have enough destination tokens
3. The user who initiates exchange has to have enough source tokens and approve them to exchange contract

### Commands
Install all packages calling `npm install` first

Compile to Truffle standard output JSON with source code and ABI
```
npm run compile
```

Run a Solium linter
```
npm run lint
```


Run tests suites
```
npm test
```