pragma solidity 0.5.0;

import "../node_modules/openzeppelin-solidity/contracts/token/ERC20/ERC20.sol";
import "../node_modules/openzeppelin-solidity/contracts/math/SafeMath.sol";

contract TokenExchange {
    using SafeMath for uint256;

    struct TokenSpecification {
        uint256 etherRate;
        address tokenOwner;
        bool available;
    }

    mapping(address => TokenSpecification) public supportedTokens;

    function exchange(address fromToken, address toToken, uint256 amount)
        external
    {
        TokenSpecification storage sourceTokenSpec = supportedTokens[fromToken];
        TokenSpecification storage destinationTokenSpec = supportedTokens[toToken];

        require(
            sourceTokenSpec.available && destinationTokenSpec.available,
            "unsupported token"
        );

        ERC20 sourceContract = ERC20(fromToken);
        ERC20 destinationContract = ERC20(toToken);
        address thisContract = address(this);

        require(
            sourceContract.balanceOf(msg.sender) >= amount,
            "insufficient token amount"
        );

        require(
            sourceContract.allowance(msg.sender, thisContract) >= amount, 
            "not enough tokens are allowed"
        );

        uint256 calculatedTokens = amount
            .mul(sourceTokenSpec.etherRate)
            .div(destinationTokenSpec.etherRate);

        require(
            destinationContract.balanceOf(thisContract) >= calculatedTokens, 
            "insufficient tokens in exchange pool"
        );

        sourceContract.transferFrom(msg.sender, thisContract, amount);
        destinationContract.transfer(msg.sender, calculatedTokens);
    }

    function addSupportedToken(address tokenContractAddress, uint256 etherRate)
        external
    {
        require(
            !supportedTokens[tokenContractAddress].available,
            "token is alredy supported"
        );

        supportedTokens[tokenContractAddress] = TokenSpecification(
            etherRate,
            msg.sender,
            true
        );

        _pullTokens(tokenContractAddress);
    }

    function pullTokens(address tokenContractAddress)
        external
    {
        require(
            supportedTokens[tokenContractAddress].tokenOwner == msg.sender, 
            "only token owner may execute this command"
        );

        _pullTokens(tokenContractAddress);
    }

    function _pullTokens(address tokenContractAddress)
        internal
    {
        TokenSpecification storage tokenSpec = supportedTokens[tokenContractAddress];
        ERC20 tokenContract = ERC20(tokenContractAddress);
        address thisContract = address(this);

        uint256 allowedTokens = tokenContract.allowance(
            tokenSpec.tokenOwner,
            thisContract
        );
        tokenContract.transferFrom(
            tokenSpec.tokenOwner,
            thisContract,
            allowedTokens
        );
    }
}