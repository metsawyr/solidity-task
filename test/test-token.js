const TestToken = artifacts.require('./TestToken.sol');
const { BigNumber } = require('bignumber.js');

contract('TestToken', (accounts) => {
    let tt1Instance;
    let tt2Instance;

    beforeEach(async () => {
        tt1Instance = await TestToken.new(
            'Test token 1',
            'TT1',
            { from: accounts[0] }
        );
        tt2Instance = await TestToken.new(
            'Test token 2',
            'TT2',
            { from: accounts[1] }
        );
    });

    it('should supply first account with 100 TT1s', async () => {
        const balance = await tt1Instance.balanceOf.call(accounts[0]);

        assert.equal(
            balance.toString(),
            new BigNumber('100e18').toString()
        );
    });

    it('should supply second account with 100 TT2s', async () => {
        const balance = await tt2Instance.balanceOf.call(accounts[1]);

        assert.equal(
            balance.toString(),
            new BigNumber('100e18').toString()
        );
    });

    it('should transfer 10 tokens from first to third account', async () => {
        await tt1Instance.transfer(
            accounts[2],
            new BigNumber('10e18').toString(),
            { from: accounts[0] }
        );

        let account1Balance = await tt1Instance.balanceOf.call(accounts[0]);
        let account3Balance = await tt1Instance.balanceOf.call(accounts[2]);
        let totalSupply = await tt1Instance.totalSupply.call();

        assert.equal(
            account1Balance.toString(),
            new BigNumber('90e18').toString()
        );

        assert.equal(
            account3Balance.toString(),
            new BigNumber('10e18').toString()
        );

        assert.equal(
            totalSupply.toString(),
            new BigNumber('100e18').toString()
        )
    });

    it('should transfer 10 tokens from second to fourth account', async () => {
        await tt2Instance.transfer(
            accounts[3],
            new BigNumber('10e18').toString(),
            { from: accounts[1] }
        );

        let account2Balance = await tt2Instance.balanceOf.call(accounts[1]);
        let account4Balance = await tt2Instance.balanceOf.call(accounts[3]);
        let totalSupply = await tt2Instance.totalSupply.call();

        assert.equal(
            account2Balance.toString(),
            new BigNumber('90e18').toString()
        );

        assert.equal(
            account4Balance.toString(),
            new BigNumber('10e18').toString()
        );

        assert.equal(
            totalSupply.toString(),
            new BigNumber('100e18').toString()
        )
    });
})