const TestToken = artifacts.require('./TestToken.sol');
const TokenExchange = artifacts.require('./TokenExchange.sol');
const { BigNumber } = require('bignumber.js');

contract('TokenExchange', (accounts) => {
    let tt1Instance;
    let tt2Instance;
    let exchangeInstance;

    beforeEach(async () => {
        tt1Instance = await TestToken.new(
            'Test token 1',
            'TT1',
            { from: accounts[0] }
        );

        tt2Instance = await TestToken.new(
            'Test token 2',
            'TT2',
            { from: accounts[1] }
        );

        exchangeInstance = await TokenExchange.new();
    });

    it('should add support for new token', async () => {
        await exchangeInstance.addSupportedToken(
            tt1Instance.address,
            new BigNumber('0.5e18').toString(),
            { from: accounts[0] }
        );

        const supportedToken = await exchangeInstance.supportedTokens.call(
            tt1Instance.address
        );

        assert.equal(
            supportedToken.etherRate.toString(),
            new BigNumber('0.5e18').toString()
        );

        assert.equal(
            supportedToken.tokenOwner,
            accounts[0]
        );

        assert.equal(
            supportedToken.available,
            true
        );

    });

    it('should execute the exchange from TT1 into TT2', async () => {
        await addSupportForTokens();

        await supplyBuyerWithTokens('15e18');

        await supplyExchangeContractWithDestinationTokens('50e18');

        await approveTokensForExchangeContractByBuyer('15e18');

        await exchangeInstance.exchange(
            tt1Instance.address,
            tt2Instance.address,
            new BigNumber('15e18').toString(),
            { from: accounts[2] }
        );

        const buyerTt1Balance = await tt1Instance.balanceOf.call(accounts[2]);
        const buyerTt2Balance = await tt2Instance.balanceOf.call(accounts[2]);
        const exchangeTt1Balance = await tt1Instance.balanceOf.call(exchangeInstance.address);
        const exchangeTt2Balance = await tt2Instance.balanceOf.call(exchangeInstance.address);

        assert.equal(
            buyerTt1Balance.toString(),
            '0'
        );

        assert.equal(
            buyerTt2Balance.toString(),
            new BigNumber('30e18').toString()
        );

        assert.equal(
            exchangeTt1Balance.toString(),
            new BigNumber('15e18').toString()
        );

        assert.equal(
            exchangeTt2Balance.toString(),
            new BigNumber('20e18').toString()
        );
    });

    it('should throw if token in not supported', async () => {
        await supplyBuyerWithTokens('15e18');

        await supplyExchangeContractWithDestinationTokens('50e18');

        await approveTokensForExchangeContractByBuyer('15e18');

        try {
            await exchangeInstance.exchange(
                tt1Instance.address,
                tt2Instance.address,
                new BigNumber('15e18').toString(),
                { from: accounts[2] }
            );
        } 
        catch (e) {
            assert(e.message.indexOf('unsupported token') > -1);
            return;
        }

        assert.fail();
    });

    it('should throw if buyer has not enough tokens', async () => {
        await addSupportForTokens();

        await supplyBuyerWithTokens('10e18');

        await supplyExchangeContractWithDestinationTokens('50e18');

        await approveTokensForExchangeContractByBuyer('15e18');

        try {
            await exchangeInstance.exchange(
                tt1Instance.address,
                tt2Instance.address,
                new BigNumber('15e18').toString(),
                { from: accounts[2] }
            );
        } 
        catch (e) {
            assert(e.message.indexOf('insufficient token amount') > -1);
            return;
        }

        assert.fail();
    });

    it('should throw if buyer approved not enough tokens to TokenExchange contract', async () => {
        await addSupportForTokens();

        await supplyBuyerWithTokens('15e18');

        await supplyExchangeContractWithDestinationTokens('50e18');

        await approveTokensForExchangeContractByBuyer('10e18');

        try {
            await exchangeInstance.exchange(
                tt1Instance.address,
                tt2Instance.address,
                new BigNumber('15e18').toString(),
                { from: accounts[2] }
            );
        } 
        catch (e) {
            assert(e.message.indexOf('not enough tokens are allowed') > -1);
            return;
        }

        assert.fail();
    });

    it('should throw if TokenExchange contract has insufficient amount of destination tokens', async () => {
        await addSupportForTokens();

        await supplyBuyerWithTokens('15e18');

        await supplyExchangeContractWithDestinationTokens('10e18');

        await approveTokensForExchangeContractByBuyer('15e18');

        try {
            await exchangeInstance.exchange(
                tt1Instance.address,
                tt2Instance.address,
                new BigNumber('15e18').toString(),
                { from: accounts[2] }
            );
        } 
        catch (e) {
            assert(e.message.indexOf('insufficient tokens in exchange pool') > -1);
            return;
        }

        assert.fail();
    });

    // Helper functions to avoid tons of copy/pasted code in test suites

    async function addSupportForTokens() {
        await exchangeInstance.addSupportedToken(
            tt1Instance.address,
            new BigNumber('0.5e18').toString(),
            { from: accounts[0] }
        );

        await exchangeInstance.addSupportedToken(
            tt2Instance.address,
            new BigNumber('0.25e18').toString(),
            { from: accounts[1] }
        );
    }

    async function supplyBuyerWithTokens(amount) {
        await tt1Instance.transfer(
            accounts[2],
            new BigNumber(amount).toString(),
            { from: accounts[0] }
        );
    }

    async function supplyExchangeContractWithDestinationTokens(amount) {
        await tt2Instance.transfer(
            exchangeInstance.address,
            new BigNumber(amount).toString(),
            { from: accounts[1] }
        );
    }

    async function supplyExchangeContractWithDestinationTokens(amount) {
        await tt2Instance.transfer(
            exchangeInstance.address,
            new BigNumber(amount).toString(),
            { from: accounts[1] }
        );
    }

    async function approveTokensForExchangeContractByBuyer(amount) {
        await tt1Instance.approve(
            exchangeInstance.address,
            new BigNumber(amount).toString(),
            { from: accounts[2] }
        );
    }
})